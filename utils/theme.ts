const theme = {
  colors: {
    lightGrey: '#F4F7FA',
    lightText: '#333333',
    grey: '#DFE1EC',
    darkRed: '#ED5565',
    darkBlue: '#5580ED',
    green: '#A6FFCC',
    black: '#000000',
    white: '#FFFFFF',
  },
};

export default theme;
