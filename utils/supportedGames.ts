const supportedGames = [
  'lol',
] as const;

export type SupportedGames = typeof supportedGames[number];

export default supportedGames;
