type Zero = 0;
type Pixels = `${number}px`;
type Percent = `${number}%`;

type PixelsOrPercent = Pixels | Percent | Zero;

type BorderRadius =
| `${PixelsOrPercent} ${PixelsOrPercent} ${PixelsOrPercent} ${PixelsOrPercent}`
| `${PixelsOrPercent} ${PixelsOrPercent}`
| `${PixelsOrPercent}`;
