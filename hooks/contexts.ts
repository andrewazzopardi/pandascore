import { createContext, useContext } from 'react';

const NoOp = () => {};

export type StatsContextType<T = any> = {
  current: T;
  history: T[];
  selectedStat: string;
  setSelectedStat: (nextStat: string) => void;
};

const StatsContext = createContext<StatsContextType>({
  current: {},
  history: [],
  selectedStat: '',
  setSelectedStat: NoOp,
});

const useLolStatsContext = () => useContext<StatsContextType<LolFrame>>(StatsContext);

export {
  StatsContext,
  useLolStatsContext,
};
