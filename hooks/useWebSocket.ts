import {
  useCallback,
  useEffect, useRef, useState,
} from 'react';

function useWebsocket<TMessage>(url: string, callback: (data: TMessage) => void) {
  const [connected, setConnected] = useState(false);
  const [error, setError] = useState(false);
  const [retryIn, setRetryIn] = useState(5);
  const ws = useRef(null);
  const retryIntervalRef = useRef<number | null>(null);

  useEffect(() => {
    if (error) return () => {};

    const wsCurrent = new WebSocket(url);

    wsCurrent.onopen = () => {
      setConnected(true);
    };

    wsCurrent.onclose = () => {
      setConnected(false);
    };

    wsCurrent.addEventListener('error', () => {
      setError(true);
    });

    wsCurrent.onmessage = (e) => {
      const message = JSON.parse(e.data);
      callback(message);
    };

    ws.current = wsCurrent;

    return () => {
      wsCurrent.close();
    };
  }, [url, callback, error]);

  const retryFunction = useCallback(() => {
    setRetryIn(retryIn - 1);
    if (retryIn === 0) {
      setRetryIn(5);
      window.clearInterval(retryIntervalRef.current);
      setError(false);
    }
  }, [retryIn]);

  useEffect(() => {
    if (error) {
      retryIntervalRef.current = window.setTimeout(retryFunction, 1000);
    }
  }, [error, retryFunction]);

  return { connected, error, retryIn };
}

export default useWebsocket;
