import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  width: 100vw;
  min-height: 100vh;
  display: flex;
`;

type Props = {
  children: React.ReactNode;
};

function Layout({ children }: Props) {
  return (
    <Container>
      {children}
    </Container>
  );
}

export default Layout;
