import React, { useCallback, useMemo, useState } from 'react';
import { StatsContext, StatsContextType } from '../../hooks/contexts';
import useWebsocket from '../../hooks/useWebSocket';
import { SupportedGames } from '../../utils/supportedGames';

import LoLLeaderboard from './LoL/Leaderboard';

const leaderboards = {
  lol: LoLLeaderboard,
};

type Props = {
  gameId: SupportedGames;
};

function Stats({ gameId }: Props) {
  const [history, setHistory] = useState<LolFrame[]>([]);
  const [current, setCurrent] = useState<LolFrame>();
  const [selectedStat, setSelectedStat] = useState<string>('gold');

  const state = useMemo<StatsContextType<LolFrame>>(() => ({
    history,
    current,
    selectedStat,
    setSelectedStat,
  }), [history, current, selectedStat]);

  const onMessage = useCallback((data: LolFrame) => {
    setCurrent(data);
    setHistory((curr) => [...curr, data]);
  }, []);

  const { connected, error, retryIn } = useWebsocket<LolFrame>('ws://localhost:4000', onMessage);

  if (error) {
    return (
      <div>
        There was an issue connecting to the websocket.
        Please make sure you have a websocket server running on your localhost port 4000.
        Will retry to connect in
        {' '}
        {retryIn}
        {' '}
        seconds.
      </div>
    );
  }

  if (!connected) {
    return <div>Connecting...</div>;
  }

  if (!state.current) {
    return <div>Connected... Waiting for statistics.</div>;
  }

  const Leaderboard = leaderboards[gameId];

  return (
    <StatsContext.Provider value={state}>
      <Leaderboard />
    </StatsContext.Provider>
  );
}

export default Stats;
