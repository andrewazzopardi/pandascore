import React from 'react';
import styled, { keyframes } from 'styled-components';

const ScrollingBlockWrapper = styled.div`
  overflow: hidden;
  display: flex;
`;

const scrollingAnimation = (team: 'red' | 'blue') => keyframes`
  0% {
    transform: translateX(${team === 'red' ? '-' : ''}100%);
  }
  100% {
    transform: translateX(0%);
  }
`;

type ScrollingProps = {
  team: 'red' | 'blue';
};

const Scrolling = styled.div<ScrollingProps>`
  display: flex;
  grid-gap: 24px;

  align-items: center;
  justify-content: space-evenly;

  padding: 0 16px;

  animation: ${({ team }) => scrollingAnimation(team)} 1s linear forwards;
`;

type Props = {
  children: React.ReactNode;
  team: 'red' | 'blue';
};

function ScrollingBlock({ children, team }: Props) {
  return (
    <ScrollingBlockWrapper>
      <Scrolling team={team}>
        {children}
      </Scrolling>
    </ScrollingBlockWrapper>
  );
}

export default ScrollingBlock;
