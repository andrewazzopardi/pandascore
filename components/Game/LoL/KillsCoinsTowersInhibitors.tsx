import Image from 'next/image';
import React from 'react';
import styled, { keyframes } from 'styled-components';
import { useLolStatsContext } from '../../../hooks/contexts';
import Block from '../../Block';
import Indicator from './Indicator';

const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr auto 1fr;

  align-items: center;
  justify-content: center;

  margin-top: 32px;
`;

const KillsBlock = styled(Block)`
  & > div {
    height: 48px;
    width: 120px;

    display: flex;
    align-items: center;
    justify-content: space-evenly;
    cursor: pointer;

    transition: transform 200ms;

    &:hover {
      transform: scale(1.1);
    }
  }
`;

const ScrollingBlockWrapper = styled(Block)`
  overflow: hidden;
`;

const scrollingAnimation = (team: 'red' | 'blue') => keyframes`
  0% {
    transform: translateX(${team === 'red' ? '-' : ''}100%);
  }
  100% {
    transform: translateX(0%);
  }
`;

const ScrollingBlock = styled<any>(Block)`
  display: flex;
  grid-gap: 24px;

  align-items: center;
  justify-content: space-evenly;

  height: 40px;
  padding: 0 16px;

  animation: ${({ team }) => scrollingAnimation(team)} 1s linear forwards;
`;

const blueIndicators: LolTeamStatIndicator[] = ['inhibitors', 'towers', 'gold'];
const redIndicators: LolTeamStatIndicator[] = [...blueIndicators].reverse();

function KillsCoinsTowersInhibitors() {
  const { current, setSelectedStat } = useLolStatsContext();

  return (
    <Container>
      <ScrollingBlockWrapper>
        <ScrollingBlock team="blue" color="lightGrey">
          {blueIndicators.map((statId) => (
            <Indicator key={statId} statId={statId} team="blue" />
          ))}
        </ScrollingBlock>
      </ScrollingBlockWrapper>
      <KillsBlock color="grey" borderRadius="3px" onClick={() => setSelectedStat('kills')}>
        <div>
          {current.blue.kills}
          <Image src="/icons/lol/kills.svg" width={20} height={20} />
          {current.red.kills}
        </div>
      </KillsBlock>
      <ScrollingBlockWrapper>
        <ScrollingBlock team="red" color="lightGrey">
          {redIndicators.map((statId) => (
            <Indicator key={statId} statId={statId} team="red" />
          ))}
        </ScrollingBlock>
      </ScrollingBlockWrapper>
    </Container>
  );
}

export default KillsCoinsTowersInhibitors;
