import React from 'react';
import styled, { keyframes } from 'styled-components';
import { useLolStatsContext } from '../../../hooks/contexts';
import fromSecondsToTime from '../../../utils/time';
import Block from '../../Block';
import Indicator from './Indicator';
import ScrollingBlock from './ScrollingBlock';

const ContainerWrapper = styled.div`
  overflow: hidden;
`;

const scrollDownAnimation = keyframes`
  0% {
    transform: translateY(-100%);
  }
  100% {
    transform: translateY(0);
  }
`;

const Container = styled.div`
  display: flex;
  justify-content: center;

  height: 25px;

  margin-top: 4px;

  animation: ${scrollDownAnimation} 1s linear forwards;
`;

const TimeBlock = styled(Block)`
  display: grid;
  place-items: center;

  height: 25px;
  width: 80px;
`;

const blueIndicators: LolTeamStatIndicator[] = ['herald', 'nashors', '', 'drakes'];
const redIndicators: LolTeamStatIndicator[] = [...blueIndicators].reverse();

function TimeAndExtraInfo() {
  const { current } = useLolStatsContext();

  return (
    <ContainerWrapper>
      <Container>
        <ScrollingBlock team="blue">
          {blueIndicators.map((indicator) => (
            <Indicator key={indicator} team="blue" statId={indicator} />
          ))}
        </ScrollingBlock>
        <TimeBlock color="green" borderRadius="3px">
          {fromSecondsToTime(current.current_timestamp)}
        </TimeBlock>
        <ScrollingBlock team="red">
          {redIndicators.map((indicator) => (
            <Indicator key={indicator} team="red" statId={indicator} />
          ))}
        </ScrollingBlock>
      </Container>
    </ContainerWrapper>
  );
}

export default TimeAndExtraInfo;
