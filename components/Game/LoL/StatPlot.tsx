import dynamic from 'next/dynamic';
import { Data } from 'plotly.js';
import React, { useMemo } from 'react';
import { useLolStatsContext } from '../../../hooks/contexts';
import fromSecondsToTime from '../../../utils/time';

const Plot = dynamic(() => import('react-plotly.js'), { ssr: false });

const teams = ['blue', 'red'] as const;

function StatPlot() {
  const { history, selectedStat } = useLolStatsContext();

  const plotData = useMemo(() => {
    const x = history.map((frame) => fromSecondsToTime(frame.current_timestamp));
    const lines: Data[] = teams.map((team) => ({
      x,
      y: history.map((frame) => frame[team][selectedStat]),
      name: team,
      type: 'scatter',
      mode: 'lines+markers',
      line: {
        color: team,
        width: 2,
      },
      marker: {
        color: team,
        size: 4,
      },
    }));

    return lines;
  }, [history, selectedStat]);

  return (
    <div
      style={{ height: '450px' }}
    >
      <Plot
        style={{ width: '100%' }}
        data={plotData}
        layout={{
          showlegend: false,
          title: selectedStat.charAt(0).toUpperCase() + selectedStat.slice(1),
          xaxis: {
            tickangle: 0,
            nticks: 10,
          },
          yaxis: {
            rangemode: 'tozero',
          },
          margin: {
            l: 30,
            r: 0,
            b: 24,
          },
        }}
        config={{
          displayModeBar: false,
        }}
      />
    </div>
  );
}

export default StatPlot;
