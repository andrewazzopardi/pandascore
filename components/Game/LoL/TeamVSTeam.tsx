import Image from 'next/image';
import React from 'react';
import styled from 'styled-components';
import { useLolStatsContext } from '../../../hooks/contexts';
import Block from '../../Block';

const Container = styled.div`
  display: flex;
  grid-gap: 24px;

  height: 35px;
  align-items: stretch;
`;

type TeamTitleProps = {
  team: 'red' | 'blue';
};

const TeamTitle = styled.div<TeamTitleProps>`
  flex-grow: 1;
  
  display: flex;
  align-items: center;
  justify-content: ${({ team }) => (team === 'blue' ? 'flex-end' : 'flex-start')};
  cursor: default;

  background-color: ${({ theme }) => theme.colors.lightGrey};

  & > span {
    padding: 0 16px;
    font-weight: 800;
  }
`;

const VSBlock = styled.div`
  margin: 0 24px;
  display: grid;
  place-items: center;
`;

function TeamVSTeam() {
  const { current } = useLolStatsContext();

  return (
    <Container>
      <TeamTitle team="blue">
        <span>
          {current.blue.name}
        </span>
        <Block color="grey" style={{ width: 42, display: 'flex', justifyContent: 'center' }}>
          <Image src="/images/lol/blue_logo.png" width={35} height={35} />
        </Block>
      </TeamTitle>
      <VSBlock>
        VS
      </VSBlock>
      <TeamTitle team="red">
        <Block color="grey" style={{ width: 42, display: 'flex', justifyContent: 'center' }}>
          <Image src="/images/lol/red_logo.png" width={35} height={35} />
        </Block>
        <span>
          {current.red.name}
        </span>
      </TeamTitle>
    </Container>
  );
}

export default TeamVSTeam;
