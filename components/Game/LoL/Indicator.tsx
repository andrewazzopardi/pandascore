import Image from 'next/image';
import React from 'react';
import styled from 'styled-components';
import { useLolStatsContext } from '../../../hooks/contexts';
import Number from '../../Number';

const Container = styled.div`
  display: flex;
  grid-gap: 8px;
  min-width: 30px;
  cursor: pointer;

  transition: transform 200ms;

  & > * {
    flex-shrink: 0;
  }

  &:hover {
    transform: scale(1.1);
  }
`;

type Props = {
  statId: LolTeamStatIndicator;
  team: 'blue' | 'red';
};

function Indicator({ statId, team }: Props) {
  const { current, setSelectedStat } = useLolStatsContext();

  if (statId === '') {
    return <Container />;
  }

  return (
    <Container
      onClick={() => setSelectedStat(statId)}
    >
      {team === 'blue' ? <Number value={current[team][statId]} /> : null}
      <Image src={`/icons/lol/${statId}.svg`} width={15} height={15} style={{ filter: team === 'blue' ? 'hue-rotate(-120deg)' : '' }} />
      {team === 'red' ? <Number value={current[team][statId]} /> : null}
    </Container>
  );
}

export default Indicator;
