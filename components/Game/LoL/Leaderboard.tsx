import React from 'react';
import styled from 'styled-components';

import KillsCoinsTowersInhibitors from './KillsCoinsTowersInhibitors';
import StatPlot from './StatPlot';
import TeamVSTeam from './TeamVSTeam';
import TimeAndExtraInfo from './TimeAndExtraInfo';

const Container = styled.div`
  width: 550px;
`;

function Leaderboard() {
  return (
    <Container>
      <TeamVSTeam />
      <KillsCoinsTowersInhibitors />
      <TimeAndExtraInfo />
      <div style={{ margin: '8px 0', textAlign: 'center' }}>
        Click on the statistics icons to change the plot to that statistic.
      </div>
      <StatPlot />
      <div style={{
        margin: '40px 0 0',
        textAlign: 'center',
      }}
      >
        Historic data is not shown. Only live data is shown.
      </div>
    </Container>
  );
}

export default Leaderboard;
