import React from 'react';

const numberSuffixes = ['', 'K', 'M', 'B', 'T'];

type Props = {
  value: number;
};

function Number({ value }: Props) {
  const suffix = numberSuffixes[Math.floor(Math.log10(value) / 3)];
  const number = value / 10 ** (3 * (suffix ? 1 : 0));

  return (
    <div>
      {number}
      {suffix}
    </div>
  );
}

export default Number;
