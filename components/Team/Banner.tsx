import React from 'react';
import styled from 'styled-components';
import Block from '../Block';

type ContainerProps = {
};

const Container = styled.div<ContainerProps>`
  display: flex;
  flex-direction: column;
  text-align: center;
`;

type Props = {
  align?: 'left' | 'right';
};

function Banner({ align = 'left' }: Props) {
  return (
    <Container>
      <Block>
        Team 1
        {' '}
        {align}
      </Block>
      <Block>Team Logo</Block>
    </Container>
  );
}

export default Banner;
