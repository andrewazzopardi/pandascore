import styled from 'styled-components';
import { Theme } from '../styled';

type Props = {
  borderRadius?: BorderRadius;
  color?: keyof Theme['colors'];
};

const Block = styled.div<Props>`
  cursor: default;

  ${({ borderRadius }) => borderRadius && `
    border-radius: ${borderRadius};
  `}
  ${({ color, theme }) => color && `
    background-color: ${theme.colors[color]};
  `}
`;

export default Block;
