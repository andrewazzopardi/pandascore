import React from 'react';
import styled from 'styled-components';
import Stats from '../components/Game/Stats';

const Container = styled.div`
  display: grid;
  place-items: center;
  flex: 1;
`;

function IndexPage() {
  return (
    <Container>
      <Stats gameId="lol" />
    </Container>
  );
}

export default IndexPage;
